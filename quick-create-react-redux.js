const path = require('path')
const fs = require('fs-extra')
const sh = require('shelljs')

console.log('\x1b[1mThanks for using quick-react-redux-app!\x1b[0m\n')

console.log('Copying project files...')
fs.copySync(path.resolve(__dirname, 'files'), path.resolve('./'))

console.log('Installing and building...')
sh.exec(
  `cd "${path.resolve('./')}" && npm install && npm run build`
)

console.log('\n\nReady to go. Check the README for more info.')
