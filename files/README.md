Setup:

```sh
npm install
```

To run in development mode:

```sh
node .
```

To build for the web:

```sh
npm run build
```

