module.exports = {
  title: 'Sample Application',
  description: 'Description of my sample application',
  state: {
    todos: [
      {
        text: 'Option 1',
        checked: true
      }, {
        text: 'Option 2',
        checked: false
      }, {
        text: 'Option 3',
        checked: false
      }
    ]
  }
}
