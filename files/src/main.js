import React from 'react'
import { render } from 'react-dom'
import { Provider, connect } from 'react-redux'
import * as redux from 'redux'

import { state as defaultState } from './default.js'

import Main from './main.jsx'

const splice = (array, start, deleteCount, ...items) => {
  const newArray = array.concat()
  newArray.splice(start, deleteCount, ...items)
  return newArray
}

const mainReducer = (state = defaultState, action) => {
  const { type, payload } = action
  switch (type) {
    case 'TODO_CHECKED':
      return {
        ...state,
        todos: splice(state.todos, payload.index, 1, {
          ...state.todos[payload.index],
          checked: payload.isChecked
        })
      }
  }
  return state
}

const store = redux.createStore(
  mainReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const ConnectedMain = connect(
  // Map state to props
  state => {
    return state
  },
  // Map dispatch to props
  dispatch => ({
    onChecked: (index, isChecked) => dispatch({
      type: 'TODO_CHECKED',
      payload: {
        index,
        isChecked
      }
    })
  })
)(Main)

render(
  <Provider store={store}>
    <ConnectedMain />
  </Provider>,
  document.querySelector('.app-wrapper')
)
