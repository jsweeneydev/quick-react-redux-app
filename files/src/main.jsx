import React from 'react'

export default props => {
  const { todos, onChecked } = props

  return <div>
    <img src='./static/image.png' />
    <h1>Sample App</h1>
    {todos.map((todo, index) => <div
      key={index}
      className={todo.checked ? 'highlighted' : null}
    >
      <input
        id={`todo-${index}`}
        type='checkbox'
        defaultChecked={todo.checked}
        onChange={e => { onChecked(index, e.target.checked) }}
      />
      <label htmlFor={`todo-${index}`}>{todo.text}</label>
    </div>)}
  </div>
}
