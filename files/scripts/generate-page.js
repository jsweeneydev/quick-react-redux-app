const buildConfig = require('./config.js')
require('@babel/register')(buildConfig.babel)

const ReactDOMServer = require('react-dom/server')
const fs = require('fs')
const path = require('path')
const baseHtml = fs.readFileSync(path.resolve(__dirname, './base.html'), 'utf8')

// Limitation: the base "generated" jsx and default.js won't update until the
// server is restarted, but that's probably tolerable
const pageDefaults = require('../src/default.js')
const generateMainPage = () => {
  return require('../src/main.jsx').default(pageDefaults.state)
}

module.exports = (config = { includeReload: false }) => {
  return baseHtml
    .replace(
      '%_TITLE_%',
      pageDefaults.title || 'Home'
    )
    .replace(
      '%_DESCRIPTION_%',
      pageDefaults.description || 'Site Description'
    )
    .replace(
      '%_BODY_%',
      ReactDOMServer.renderToStaticMarkup(generateMainPage())
    )
    .replace(
      '%_RELOAD_%',
      config.includeReload ? '<script src="/reload/reload.js"></script>' : ''
    )
}
