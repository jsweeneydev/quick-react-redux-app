const express = require('express')
const http = require('http')
const webpack = require('webpack')
const setupWebpackMiddleware = require('webpack-dev-middleware')
const path = require('path')
const reload = require('reload')
const watch = require('watch')

const buildConfig = require('./config.js')
const generatePage = require('./generate-page.js')

const port = buildConfig.port

const app = express()
const webpackMiddleware = setupWebpackMiddleware(webpack({
  ...buildConfig.webpack,
  mode: 'development',
  output: {
    path: '/',
    filename: 'script.js',
    devtoolModuleFilenameTemplate: info => info.resourcePath
  }
}))
app.use(webpackMiddleware)
app.get('/', (req, res) => {
  res.status(200).send(generatePage({ includeReload: true }))
})
;[
  'style.css',
  'favicon.png',
  'favicon.ico'
].forEach(file => {
  app.get(`/${file}`, (req, res) => {
    res.status(200).sendFile(path.resolve(`./src/${file}`))
  })
})
app.use('/static', express.static('./src/static'))

const server = http.createServer(app)
app.set('port', port)
server.listen(app.get('port'), () => {
  console.log(`Web server listening on port ${port}`)
})
// Reload the page if the script changes
reload(app, { verbose: true })
  .then(reload => {
    let lastScript = null
    setInterval(() => {
      const script = webpackMiddleware.fileSystem.data['script.js']
      if (lastScript !== script) {
        lastScript = script
        reload.reload()
      }
    }, 50)
    // Reload the page if any non-script-related files change
    watch.createMonitor('./src/', monitor => {
      monitor.on('changed', f => {
        if (!(/.*.jsx?$/.test(f))) {
          reload.reload()
        }
      })
    })
  })
