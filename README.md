_Note: due to limitations in preact-compat that make it incompatible with [react-redux 6.0.0](https://github.com/reduxjs/react-redux/releases/tag/v6.0.0), preact has been removed from the boilerplate, at least for now._

-----

Creates the scripts necessary to develop and publish a lightweight ~~[Preact](https://preactjs.com/)~~ [React](https://reactjs.org/) + [Redux](https://redux.js.org/) web app. This tool focuses primarily on:

* quick load times
  * ~~uses [Preact](https://preactjs.com/) instead of React in production~~
  * generates static page for immediate page functionality
  * ~~relatively small footprint  (bundled script for the sample project is 44 KB)~~
* fast development
  * running/rebuilding in-memory with [webpack-dev-middleware](https://www.npmjs.com/package/webpack-dev-middleware) and [reload](https://www.npmjs.com/package/reload)
  * generates production-ready html, no "exporting" required
* limited boilerplate
  * only includes scripts for running in development and building for production
  * allows for faster download/development with more customization



#### Alternatives

This tool focuses on laying only the requisite foundation for react-redux web apps. Depending on your use case, you might want to use something more specialized:

* [create-react-app](https://github.com/facebook/create-react-app) if you intend to transfer this into an existing app or load times are less important
* [gatsby](https://www.gatsbyjs.org/) if more boilerplate/foundation is preferred
* [fastpage](https://github.com/ReAlign/fastpage/blob/master/README_en.md)
* [add-redux](https://www.npmjs.com/package/add-redux) for a pure focus on redux
* _and much more_



#### Usage

Just run this in the console:

```
npx quick-react-redux-app
```

This will install and build a small sample application structure.

The file tree created is as follows:

```
scripts
|- base.html
|- build.js
|- config.js
|- generate-page.js
|- run.js
src
|- default.js
|- favicon.ico
|- favicon.png
|- main.js
|- main.jsx
|- style.css
|- static
   |- image.png
.gitignore
package.json
README.md
```

You should only need to worry about the files in the `src` folder.

**`default.js`**

Defines the title of the web page (`title`), the meta description (`description`), and the initial state of the redux store (`state`).

**`favicon.ico`**

Sample default 16x16 favicon.

**`favicon.png`**

Sample larger favicon (128x128) for iPhone, etc..

**`main.js`**

Entry point for the page's scripts. By default, the primary redux reducer and store is defined here, as well as the [react-redux](https://www.npmjs.com/package/react-redux) connector.

**`main.jsx`**

Entry point for the web page.

**`style.css`**

Web page's style sheet.

**`static`**

Static files, which can be requested by "./static/" in the web page. An image is included as an example. If your project does not need any static files, this folder can safely be removed.




To run the web app locally, run node in the created project's root level:

```sh
node .
```

To build for production, run build to create an "index" folder:

```sh
npm run build
```

The created "index" folder can be dropped directly on a web server via FTP.





#### Advanced Usage

The files in the `scripts` folder handle running the application in development and building for production.

**`base.html`**

The HTML template that wraps around the page generated in the `main.jsx` file. Add any \<meta\> headers or any other non-page-body things to this file. Uses lazy text replacement during build to prepare the final page:

* `%_TITLE_%` is replaced with the title specified in the `default.js` file
* `%_DESCRIPTION_%` is replaced with the description specified in the `default.js` file
* `%_BODY_%` is replaced with the page generated in `main.jsx`
* `%_RELOAD_%` is the url to the reload script during development and an empty string in production

**`build.js`**

Generates the static webpage, builds the script bundle + source map, and copies over the css. This script is executed when running `npm run build`.

**`config.js`**

Defines the [babel](https://babeljs.io/docs/en/configuration) and [webpack](https://webpack.js.org/configuration/) configuration. Also includes the port to use when running the  web app in development.

**`generate-page.js`**

Converts the page structured in `main.jsx` and defined in `default.js` into static html.

**`run.js`**

Runs a local [express](https://expressjs.com/) web server for developing the web app. This script is executed when running `node .`.
